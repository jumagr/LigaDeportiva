from .models import Jugador, Amonestacion, Goleadores
from apps.incidencias.models import Partido, Resultados
from apps.torneo.models import PrecioPago

#creamos un objeto de tipo Sancion
class Sancion():
    amonestacion = None
    monto = 0

class Goles():
    jugador = None
    num_goles = 0


#funcionq ue devuelve un alista de amonetacion y precio
def Amonestacion_Jugador(jugador):
    amonestacion = Amonestacion.objects.filter(jugador=jugador, condicion='A')
    monto = PrecioPago.objects.get(concepto='amonestacion').monto
    lista = []
    total = 0
    #recorremos amonestacio
    for a in amonestacion:
        sub_total = a.peso_amonestacion * monto
        total = total + sub_total
        #creamos la clase
        s = Sancion()
        s.amonestacion = a
        s.monto = sub_total
        lista.append(s)
    return lista, total


def Contar_Goles(jugador, lista):
    #contamos los goles de un jugador en la lista
    goles = 0
    for j in lista:
        if j.jugador == jugador:
            goles = goles + j.num_goles
    return goles


def Goles_Jugador():
    lista = Goleadores.objects.filter(
        partido__fixture__torneo__culminado=False,
    )
    print 'esta es la lista de goleadores'
    print lista
    #creamos un lista que devovlera goles y jugador
    list_resultado = []
    list_jugadores = []
    if lista.count() > 0:
        list_jugadores.append(lista[0].jugador)
        for g in lista:
            if g.jugador not in list_jugadores:
                list_jugadores.append(g.jugador)
            else:
                print 'ya esta en la lista'
        # calculamos los goles
        for j in list_jugadores:
            goles = Goles()
            goles.jugador = j
            goles.num_goles = Contar_Goles(j,lista)
            list_resultado.append(goles)
    return list_resultado
