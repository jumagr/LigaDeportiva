# -*- encoding: utf-8 -*-
from django import forms
from django.forms.models import ModelMultipleChoiceField
from .models import Jugador, Goleadores, Amonestacion
from apps.incidencias.models import Partido
from apps.equipo.models import Equipo,JuntaDirectiva
from apps.torneo.forms import PersonaForm

class BuscarForm(forms.Form):
    CONSULTA_CHOICES = (
        ('0','aptos'),
        ('1', 'sancionados'),
        ('2', 'con goles'),
    )
    clave = forms.ChoiceField(
        label='Cosnulta Por',
        choices=CONSULTA_CHOICES,
        required=False,
    )

class ConvocadosForm(forms.Form):

    jugadores = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=None,
        required=False,
    )

    def __init__(self, user,*args, **kwargs):
        super(ConvocadosForm, self).__init__(*args, **kwargs)
        usuario = user

        Nequipo = Equipo.objects.filter(junta_directiva__presidente = usuario)

        jugadores = Jugador.objects.filter(
            equipo = Nequipo,
        )
        self.fields['jugadores'].queryset = jugadores

class GoleadorForm(forms.ModelForm):
    class Meta:
        model = Goleadores
        fields =('__all__')

    def __init__(self, pk, pa, *args, **kwargs):
        super(GoleadorForm, self).__init__(*args, **kwargs)
        equipo = Equipo.objects.get(pk=pk)
        jugadores = Jugador.objects.filter(equipo=equipo)
        partidos = Partido.objects.filter(pk=pa)
        self.fields['jugador'].queryset = jugadores
        self.fields['partido'].queryset = partidos


class GoleadorUpdateForm(forms.ModelForm):
    class Meta:
        model = Goleadores
        fields =('__all__')

class AmonestacionForm(forms.ModelForm):
    class Meta:
        model = Amonestacion
        fields = (
            'jugador',
            'tipo_amonestacion',
            'partido',
            'peso_amonestacion',
        )

    def __init__(self, pk, *args, **kwargs):
        super(AmonestacionForm, self).__init__(*args, **kwargs)
        equipo = Equipo.objects.get(pk=pk)
        jugadores = Jugador.objects.filter(equipo=equipo)
        self.fields['jugador'].queryset = jugadores


class AmonestacionUpdateForm(forms.ModelForm):
    class Meta:
        model = Amonestacion
        fields = ('__all__')


class JugadorForm(PersonaForm):
    '''clase para registra pre-matricula'''

    posicion = forms.CharField()
    numero_camiseta = forms.IntegerField()

    class Meta(PersonaForm.Meta):
        # campos q se van a mostar en el formulario pre_matricula
        fields = (
            'dni',
            'nombres',
            'apellidos',
            'email',
            'direccion',
            'sexo',
            'telefono',
            'fecha_nacimineto',
            'foto',
        )
