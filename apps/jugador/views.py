# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView
from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic.edit import FormView, FormMixin, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.utils import timezone
from apps.torneo.models import Torneo, PrecioPago, Persona
from apps.equipo.models import Equipo, Pago

from .forms import BuscarForm,ConvocadosForm,GoleadorForm, GoleadorUpdateForm
from .models import Jugador, Amonestacion, Goleadores
from apps.incidencias.models import Partido
from .forms import BuscarForm, ConvocadosForm, AmonestacionForm, JugadorForm, AmonestacionUpdateForm
from .models import Jugador, Amonestacion

from .functios import Amonestacion_Jugador, Goles_Jugador
# Create your views here.


class ListJugadores(ListView):
    context_object_name = 'goleadores'
    template_name = 'jugador/jugador/listar.html'

    def get_queryset(self):
        #recuperamos el equipo
        usuario = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=usuario)
        queryset = Goleadores.objects.filter(
            jugador__equipo=equipo,
            partido__fixture__torneo__culminado=False,
        )

        return queryset

class SeleccionJugadores(FormView):
    form_class = ConvocadosForm
    template_name = 'jugador/jugador/seleccion.html'
    success_url = reverse_lazy('jugador_app:consulta-habilitados')

    def get_form_kwargs(self):
        kwargs = super(SeleccionJugadores, self).get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
        })
        return kwargs

    def form_valid(self, form):
        # primero colocamos a todos los jugadores como ingabilitados
        usuario = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=usuario)
        jdores = Jugador.objects.filter(equipo=equipo)
        for j in jdores:
            j.estado = False
            j.save()
        #actualizamos los convocados
        LConvocados = form.cleaned_data['jugadores']
        for jugador in LConvocados:
            jugador.estado = True
            jugador.save()
        return super(SeleccionJugadores, self).form_valid(form)

class JugadoresHabilitados(ListView):
    """Lista de Jugadores con el estado Habilitado para un partido"""
    context_object_name = 'Habilitados'
    template_name = 'jugador/jugador/habilitados.html'

    def get_queryset(self):
        #recuperamos el equipo
        usuario = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=usuario)
        #recuperamos el valor por GET
        queryset = Jugador.objects.filter(equipo=equipo, estado=True)
        return queryset

class ListaEquiposToreno(ListView):
    #lista de quipos por torneo
    context_object_name = 'equipos'
    queryset = Equipo.objects.filter(estado=True).order_by('nombre')
    template_name = 'jugador/consultas/list-equipo.html'


class ListaJugadoresEquipo(DetailView):
    model = Equipo
    template_name = "jugador/consultas/list-jugadores.html"

    def get_context_data(self, **kwargs):
        context = super(ListaJugadoresEquipo, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        equipo = self.get_object()
        context['jugadores'] = Jugador.objects.filter(equipo=equipo)
        return context


class JugadorAmonestacion(DetailView):
    model = Jugador
    template_name = "jugador/consultas/amonestacion.html"

    def get_context_data(self, **kwargs):
        context = super(JugadorAmonestacion, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        jugador = self.get_object()
        a, b = Amonestacion_Jugador(jugador)
        context['monto'] = b
        context['lista'] = a
        return context


    def post(self, request, *args, **kwargs):
        jugador = self.get_object()
        junta = jugador.equipo.junta_directiva
        precio = PrecioPago.objects.filter(concepto='amonestacion')[0]
        torneo = Torneo.objects.filter(culminado=False)[0]
        usuario = self.request.user
        # guardamos el pago y actualizamos user
        pago = Pago(
            precio_pago=precio,
            junta_directiva=junta,
            torneo=torneo,
            fecha=timezone.now(),
            usuario=usuario,
        )
        #actualizamos las amonestaciones a false
        amonestacion = Amonestacion.objects.filter(jugador=jugador, condicion='A')
        for a in amonestacion:
            a.condicion = 'I'
            a.save()
        pago.save()
        return HttpResponseRedirect(
            reverse(
                'equipo_app:listar-Pago'
            )
        )
#############################################
    #Mantenimientos del Amonestacion
class ListaEquiposAmonestar(ListView):
    #lista de quipos por torneo
    context_object_name = 'equipos'
    queryset = Equipo.objects.filter(estado=True).order_by('nombre')
    template_name = 'jugador/Amonestacion/equipos.html'


class ListarAmonestacion(ListView):
    context_object_name = 'campos'
    queryset = Amonestacion.objects.all()
    template_name = 'jugador/Amonestacion/listar.html'


#clase para registrar un Amonestacion utilizando createview
class RegistrarAmonestacion(FormView):
    form_class = AmonestacionForm
    template_name = "jugador/Amonestacion/agregar.html"
    success_url = reverse_lazy('jugador_app:listar-amonestacion')

    def get_form_kwargs(self):
        kwargs = super(RegistrarAmonestacion, self).get_form_kwargs()
        kwargs.update({
            'pk': self.kwargs.get('pk', 0),
        })
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(RegistrarAmonestacion, self).form_valid(form)

#clase para ver los datos en detalle de un solo equipo
# utilizaremos un DetaiView que se encarga de recibir el id de equipo
class detalleAmonestacion(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'jugador/Amonestacion/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Amonestacion


class ModificarAmonestacion(UpdateView):
    #le especificamos la tabla de BD
    model = Amonestacion
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'jugador/Amonestacion/modificar.html'
    success_url = reverse_lazy('jugador_app:listar-amonestacion')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = AmonestacionUpdateForm


class EliminarAmonestacion(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'jugador/Amonestacion/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Amonestacion
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('jugador_app:listar-amonestacion')

# clase que mostrar la lista de goleadores del toreno
class ListaEquipoPartido(DetailView):
    model = Partido
    template_name = 'jugador/goleador/equipopartido.html'

    def get_context_data(self, **kwargs):
        context = super(ListaEquipoPartido, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        partido = self.get_object()
        context['equipo_l'] = partido.fixture.elocal
        context['equipo_v'] = partido.fixture.evisitante
        return context

class ListaGoleadores(ListView):
    context_object_name = 'jugadores'
    queryset = Goles_Jugador()
    template_name = 'jugador/consultas/goleadores.html'


# ---- CRUD GOLEADOR ----
class ListaGoleador(ListView):
    context_object_name = 'jugadores'
    queryset = Goleadores.objects.filter(partido__fixture__torneo__culminado=False)
    template_name = 'jugador/goleador/listar.html'


class RegistrarGoleadores(SuccessMessageMixin, FormView):
    form_class = GoleadorForm
    template_name = "jugador/goleador/add.html"
    success_url = '.'
    success_message = "Gool Registrado...!!"

    def get_form_kwargs(self):
        kwargs = super(RegistrarGoleadores, self).get_form_kwargs()
        kwargs.update({
            'pa': self.kwargs.get('pa', 0),
            'pk': self.kwargs.get('pk', 0),
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(RegistrarGoleadores, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        equipo= self.kwargs.get('pk', 0)
        context['equipo'] = Equipo.objects.get(pk=equipo)
        return context

    def form_valid(self, form):
        form.save()
        return super(RegistrarGoleadores, self).form_valid(form)

class ModificarGoleadores(UpdateView):
    model = Goleadores
    template_name = "jugador/goleador/update.html"
    success_url = reverse_lazy('users_app:inicio')
    form_class = GoleadorUpdateForm

class EliminarGoleadores(DeleteView):
    template_name = "jugador/goleador/delete.html"
    model = Goleadores
    success_url = reverse_lazy('users_app:inicio')

#########################
class ListJugador(ListView):
    context_object_name = 'jugadores'
    template_name = 'jugador/jugador/listarjugador.html'

    def get_queryset(self):
        #recuperamos el equipo
        usuario = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=usuario)
        #recuperamos el valor por GET
        queryset = Jugador.objects.filter(equipo=equipo)
        return queryset


#clase para registrar un equipo utilizando createview
class RegistrarJugador(FormView):
    form_class = JugadorForm
    template_name = "jugador/jugador/inscripcion.html"
    success_url = reverse_lazy('jugador_app:listar-jugador')

    def form_valid(self, form):
        # recuperamos el equipo
        presidente = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=presidente)
        # recojemos los datos de perona
        persona = Persona(
            dni=form.cleaned_data['dni'],
            nombres=form.cleaned_data['nombres'],
            apellidos=form.cleaned_data['apellidos'],
            email=form.cleaned_data['email'],
            direccion=form.cleaned_data['direccion'],
            sexo=form.cleaned_data['sexo'],
            telefono=form.cleaned_data['telefono'],
            foto=form.cleaned_data['foto'],
        )
        persona.save()
        print '=============esta es la foto'
        print persona.foto
        jugador = Jugador(
            persona=persona,
            equipo=equipo,
            posicion=form.cleaned_data['posicion'],
            numero_camiseta=form.cleaned_data['numero_camiseta'],
            estado=False,
        )
        jugador.save()
        return super(RegistrarJugador, self).form_valid(form)


class detalleJugador(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'jugador/jugador/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Jugador


class ModificarJugador(UpdateView):
    #le especificamos la tabla de BD
    model = Jugador
    template_name = 'jugador/jugador/inscripcion.html'
    success_url = reverse_lazy('jugador_app:listar-jugador')
    form_class = JugadorForm

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarJugador, self).get_initial()
        jugador = self.get_object()
        initial['dni'] = jugador.persona.dni
        initial['nombres'] = jugador.persona.nombres
        initial['apellidos'] = jugador.persona.apellidos
        initial['email'] = jugador.persona.email
        initial['direccion'] = jugador.persona.direccion
        initial['sexo'] = jugador.persona.sexo
        initial['telefono'] = jugador.persona.telefono
        initial['fecha_nacimineto'] = jugador.persona.fecha_nacimineto
        initial['foto'] = jugador.persona.foto
        initial['posicion'] = jugador.posicion
        initial['numero_camiseta'] = jugador.numero_camiseta
        return initial

    def form_valid(self, form):
        # recuperamos el equipo
        jugador = self.get_object()
        # recojemos los datos de perona
        persona = jugador.persona
        persona.dni = form.cleaned_data['dni']
        persona.nombres=form.cleaned_data['nombres']
        persona.apellidos = form.cleaned_data['apellidos']
        persona.email = form.cleaned_data['email']
        persona.direccion = form.cleaned_data['direccion']
        persona.sexo = form.cleaned_data['sexo']
        persona.telefono = form.cleaned_data['telefono']
        persona.foto = form.cleaned_data['foto']
        persona.save()
        jugador.numero_camiseta = form.cleaned_data['numero_camiseta']
        jugador.posicion = form.cleaned_data['posicion']
        jugador.save()
        return super(ModificarJugador, self).form_valid(form)


class EliminarJugador(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'jugador/jugador/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Jugador
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('jugador_app:listar-jugador')

class JugadoresAmonestados(ListView):
    context_object_name = 'amonestados'
    template_name = 'jugador/consultas/listaAmonestados.html'

    def get_queryset(self):
        #recuperamos el equipo
        usuario = self.request.user
        equipo = Equipo.objects.get(junta_directiva__presidente=usuario)
        #recuperamos el valor por GET
        queryset = Amonestacion.objects.filter(jugador__equipo=equipo, condicion='A')
        return queryset


class JugadorDeudas(DetailView):
    model = Jugador
    template_name = "jugador/consultas/deudas.html"

    def get_context_data(self, **kwargs):
        context = super(JugadorDeudas, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        jugador = self.get_object()
        a, b = Amonestacion_Jugador(jugador)
        context['monto'] = b
        context['lista'] = a
        return context
