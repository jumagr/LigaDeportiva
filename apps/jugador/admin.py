from django.contrib import admin
from .models import Jugador, Amonestacion, Goleadores
# Register your models here.

admin.site.register(Jugador)
admin.site.register(Amonestacion)
admin.site.register(Goleadores)
