from apps.equipo.models import Equipo
from apps.incidencias.models import Partido, Resultados
from apps.jugador.models import Jugador, Goleadores
from apps.users.models import User

# creamos objeto TablaPosiciones
class TablaPosiciones():
    equipo = None
    gf = 0
    gc = 0
    pj = 0
    pg = 0
    pe = 0
    pp = 0
    ptos = 0

class Goles():
    jugador = None
    num_goles = 0

def Resultados_Equipos():
    # vriable para retornar Resultados_Equipos
    lista = []
    #lista de equipos del torneo
    equipos = Equipo.objects.filter(estado=True)
    #iteramos la lista equipos
    for e in equipos:
        #creamos el objeto
        t = TablaPosiciones()
        t.equipo = e
        #obtenemos los rsultados para cada Equipo
        resul = Resultados.objects.filter(equipo=e, equipo__estado=True)
        t.pj = resul.count()
        #iteramos los resultados de estqe quipo
        for r in resul:
            t.gf = t.gf + r.goles_favor
            t.gc = t.gc + r.goles_contra
            if r.goles_favor > r.goles_contra:
                t.pg = t.pg + 1
                t.ptos = t.ptos + 3
            elif r.goles_favor == r.goles_contra:
                t.pe = t.pe + 1
                t.ptos = t.ptos + 1
            else:
                t.pp = t.pp + 1
        # agregamos el objeto a la lista
        lista.append(t)
    #devovlemos la lista
    return lista

def CulminarTorneo():
    # recuperamos la lista de equipos e inhabilitamos
    equipos = Equipo.objects.filter(estado=True)
    for e in equipos:
        # actualizamos los equipos a inhabilitados
        e.estado = False
        e.save()
        print '========se actualizo el equipo al culminar torneo=======0'
    #  recuperamos la lista de presidentes he inhabilitamos
    presidentes = User.objects.filter(type_user = '2')
    for p in presidentes:
        p.is_active = False
        print '========se actualizo el Presidente al culminar torneo=======0'
        p.save()


# creamos objeto TablaPosiciones
class EquipoPartido():
    equipo = None



def PasarEquipoLista():
    lista = []
    equipos = Equipo.objects.filter(estado=True)
    for i in equipos:
        t = EquipoPartido()
        t.equipo = i
        lista.append(t)
    return lista



