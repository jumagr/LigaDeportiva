# -*- encoding: utf-8 -*-
from django import forms

from models import CampoDeportivo, Persona, Arbitro, Fixture, PrecioPago, Torneo


class CampoDForm(forms.ModelForm):

    class Meta:
        model = CampoDeportivo
        fields = ("__all__")

    def clean_nombre(self):
        nom = self.cleaned_data['nombre']
        num_palabras = len(nom)
        if num_palabras < 5:
            raise forms.ValidationError("nombre muy corto")
        return nom

    def clean_prop(self):
        prop = self.cleaned_data['propietario']
        num_palabras = len(prop)
        if num_palabras < 5:
            raise forms.ValidationError("Nombre de Propietario muy corto")
        return prop


class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = (
            'dni',
            'nombres',
            'apellidos',
            'email',
            'direccion',
            'sexo',
            'telefono',
            'fecha_nacimineto',
            'foto',
        )
        widgets = {
            'sexo': forms.Select(attrs={'class': 'validate'}),
            'fecha_nacimineto': forms.DateInput(attrs={'class': 'datepicker'}),
            'avatar': forms.ClearableFileInput(attrs={'class': 'validate'}),
        }

    def clean_dni(self):
        documento = self.cleaned_data['dni']
        persona = Persona.objects.filter(dni=documento)
        if persona.count() > 0:
            raise forms.ValidationError("el dni ya existe")
        return documento


class TorneoForm(forms.ModelForm):
    class Meta:
        model = Torneo
        fields = ("__all__")
        widgets = {
            'fecha_inicio': forms.DateInput(attrs={'class': 'datepicker'}),
            'fecha_fin': forms.DateInput(attrs={'class': 'datepicker'}),
        }
    def clean_nombre(self):
        nom = self.cleaned_data['nombre']
        torneo = Torneo.objects.all().order_by('fecha_inicio')
        print '=============='
        print 'torneo'
        if torneo.count() > 0:
            if torneo[torneo.count()-1].culminado == False:
                raise forms.ValidationError("ya existe un torneo inciado")
        return nom

class PrecioPagoForm(forms.ModelForm):
    class Meta:
        model = PrecioPago
        fields = ("__all__")

class ArbitroForm(PersonaForm):
    experiencia = forms.IntegerField()

    class Meta(PersonaForm.Meta):
        # campos q se van a mostar en el formulario pre_matricula
        fields = (
            'dni',
            'nombres',
            'apellidos',
            'email',
            'direccion',
            'sexo',
            'telefono',
            'fecha_nacimineto',
            'foto',
        )

class CulminarForm(forms.Form):
    nombre = forms.CharField()

    def clean_nombre(self):
        name = self.cleaned_data['nombre']
        print name
        torneo = Torneo.objects.get(culminado=False)
        print torneo.nombre
        if not torneo.nombre == name:
            raise forms.ValidationError("Ingrese Nombre Correcto")
        return name
