# -*- encoding: utf-8 -*
from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.views.generic import ListView
from django.contrib.auth import authenticate, login, logout
from apps.torneo.functions import Resultados_Equipos

from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect

from apps.equipo.models import ComandoTecnico, JuntaDirectiva, Equipo

from .models import User
from .forms import LoginForm, RegistroUserForm, AdminEquipoForm
# Create your views here.


class HomeView(TemplateView):
    template_name = 'panel/frotend/frontend.html'

class PartidoView(TemplateView):
    template_name = 'panel/frotend/partido.html'

class ClasificacionView(TemplateView):
    template_name = 'panel/frotend/clasificacion.html'
    def get_context_data(self, **kwargs):
        context = super(ClasificacionView, self).get_context_data(**kwargs)
        context['resultados'] = Resultados_Equipos()
        return context

class InicioView(TemplateView):
    template_name = 'panel/panel.html'

class LogIn(FormView):
    form_class = LoginForm
    template_name = 'users/login.html'
    success_url = reverse_lazy('users_app:inicio')

    def form_valid(self, form):
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )
        if user is not None:
            if user.is_active and user.type_user == '1':
                login(self.request, user)
                return HttpResponseRedirect(
                    reverse(
                        'users_app:inicio'
                    )
                )
            elif user.is_active and user.type_user == '2':
                login(self.request, user)
                return HttpResponseRedirect(
                    reverse(
                        'users_app:panel_juntadirectiva'
                    )
                )
            else:
                return HttpResponseRedirect(
                    reverse(
                        'users_app:home'
                    )
                )


def LogOut(request):
    logout(request)
    return redirect('/')


class AgregarAdministrador(FormView):
    template_name = 'users/add-admin.html'
    form_class = RegistroUserForm
    success_url = '/'

    def form_valid(self, form):
        user = form.save()
        user.type_user = '1'
        user.set_password(form.cleaned_data['password1'])
        user.save()
        return super(AgregarAdministrador, self).form_valid(form)

    def form_invalid(self, form):
        print 'form eroors'
        return super(AgregarAdministrador, self).form_invalid(form)


class AgregarAdmiEquipo(FormView):
    template_name = 'users/admin_equipo.html'
    form_class = AdminEquipoForm
    success_url = reverse_lazy('equipo_app:registrar-Pago')

    def form_valid(self, form):
        #guardamos el usuario
        user = form.save()
        user.type_user = '2'
        user.set_password(form.cleaned_data['password1'])
        user.save()
        facultad = form.cleaned_data['facultad']
        #creamos junta directiva
        junta = JuntaDirectiva(
            presidente=user,
        )
        junta.save()
        print '==========junta guardada'
        #creamos comando tecnico
        comando = ComandoTecnico(
            junta_directiva=junta,
        )
        comando.save()
        print '==========comando guardado'
        #creamos equipo
        equipo = Equipo(
            junta_directiva=junta,
            comando_tecnico=comando,
            facultad=facultad,
        )
        equipo.save()
        return HttpResponseRedirect(
            reverse(
                'equipo_app:registrar-Pago',
                kwargs={'pk': user.pk},
            )
        )

        return super(AgregarAdmiEquipo, self).form_valid(form)

class PanelJuntaDirectiva(TemplateView):
    template_name = 'panel/paneljuntadirectiva/panel.html'


class ListAdminEquipo(ListView):
    """docstring for ListAdminEquipo"""
    #lista de administradores de equipos
    context_object_name = 'admins'
    queryset = User.objects.filter(type_user = '2').order_by('type_user')
    template_name = 'users/Lista-Admin.html'
