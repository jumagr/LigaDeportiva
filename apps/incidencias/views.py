# -*- encoding: utf-8 -*-
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView
from django.views.generic.edit import FormView, FormMixin
from django.core.urlresolvers import reverse_lazy,reverse
from django.http import HttpResponseRedirect

from .forms import ResultadosForm, BuscarForm
from apps.incidencias.models import Resultados,Partido,Insidencia, Espectador
from apps.incidencias.forms import ResultadosForm,PartidoForm,IncidenciaForm, LibroForm, EspectadorForm

from django.http import HttpResponseRedirect
from django.utils import timezone


#manteimiento Resultados
class ListResultados(ListView):
    context_object_name = 'Resultados'
    queryset = Resultados.objects.all()
    template_name = 'incidencias/Resultados/listar.html'


#clase para registrar un equipo utilizando createview
class RegistrarResultados(CreateView):
    form_class = ResultadosForm
    template_name = "incidencias/Resultados/agregar.html"
    success_url = reverse_lazy('incidencias_app:listar-Resultados')


class detalleResultados(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'incidencias/Resultados/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Resultados


class ModificarResultados(UpdateView):
    #le especificamos la tabla de BD
    model = Resultados
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'incidencias/Resultados/modificar.html'
    success_url = reverse_lazy('incidencias_app:listar-Resultados')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = ResultadosForm


class EliminarResultados(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'incidencias/Resultados/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Resultados
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('incidencias_app:listar-Resultados')

#mantenimiento Partido#######################################################################
class ListPartido(ListView):
    context_object_name = 'Partidos'
    queryset = Partido.objects.all()
    template_name = 'incidencias/Partido/listar.html'


#clase para registrar un equipo utilizando createview
class RegistrarPartido(CreateView):
    form_class = PartidoForm
    template_name = "incidencias/Partido/agregar.html"
    success_url = reverse_lazy('incidencias_app:listar-Partido')

    def form_valid(self, form):
        partido = form.save()
        equipo_l = partido.fixture.elocal
        equipo_v = partido.fixture.evisitante

        resultado1 = Resultados(
            partido=partido,
            equipo=equipo_l,
            goles_favor=partido.goles_local,
            goles_contra=partido.goles_visita,
        )
        resultado2 = Resultados(
            partido=partido,
            equipo=equipo_v,
            goles_favor=partido.goles_visita,
            goles_contra=partido.goles_local,
        )
        #guardamos los resultados
        resultado1.save()
        resultado2.save()
        return HttpResponseRedirect(
            reverse(
                'jugador_app:equipo-partido',
                kwargs={'pk': partido.pk},
            )
        )
        return super(RegistrarPartido, self).form_valid(form)


class detallePartido(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'incidencias/Partido/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Partido


class ModificarPartido(UpdateView):
    #le especificamos la tabla de BD
    model = Partido
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'incidencias/Partido/modificar.html'
    success_url = reverse_lazy('incidencias_app:listar-Partido')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = PartidoForm


class EliminarPartido(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'incidencias/Partido/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Partido
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('incidencias_app:listar-Partido')

#mantenimiento Incidencias#######################################################################
class ListIncidencia(ListView):
    context_object_name = 'Incidencias'
    queryset = Insidencia.objects.all()
    template_name = 'incidencias/Incidencia/listar.html'


#clase para registrar un equipo utilizando createview
class RegistrarIncidencia(CreateView):
    form_class = IncidenciaForm
    template_name = "incidencias/Incidencia/agregar.html"
    success_url = reverse_lazy('incidencias_app:listar-Incidencia')

    def form_valid(self, form):
        incidencia = form.save()
        incidencia.usuario = self.request.user
        incidencia.save()
        return super(RegistrarIncidencia, self).form_valid(form)


class detalleIncidencia(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'incidencias/Incidencia/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Insidencia


class ModificarIncidencia(UpdateView):
    #le especificamos la tabla de BD
    model = Insidencia
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'incidencias/Incidencia/modificar.html'
    success_url = reverse_lazy('incidencias_app:listar-Incidencia')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = IncidenciaForm


class EliminarIncidencia(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'incidencias/Incidencia/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Insidencia
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('incidencias_app:listar-Incidencia')

#######################LISTAR PARTIDOS EN FROTEND########################
class ListarPartido(ListView):
    context_object_name = 'partidos'
    queryset = Partido.objects.all()
    template_name = 'incidencias/partido_frotend/listar.html'

#######################LISTAR INCIDENCIAS EN FROTEND########################
class ListarIncidencias(ListView):
    context_object_name = 'incidencias'
    queryset = Insidencia.objects.all()
    template_name = 'incidencias/incidencia_frotend/listar.html'

############ MANTENIMIENTO ESPECTADOR ###############33
#manteimiento facultad
class ListEspectador(ListView):
    context_object_name = 'espectadores'
    template_name = 'incidencias/espectador/listar.html'
    paginate_by = 4

    def get_context_data(self, **kwargs):
        context = super(ListEspectador, self).get_context_data(**kwargs)
        context['form'] = BuscarForm
        return context

    def get_queryset(self):
        #recuperamos el valor por GET
        queryset = Espectador.objects.all()
        q = self.request.GET.get("clave")
        #utilizamos el procedimietno almacenado
        if q:
            queryset = Espectador.objects.filter(nombre__icontains=q)
        return queryset


#clase para registrar un equipo utilizando createview
class RegistrarEspectador(CreateView):
    form_class = EspectadorForm
    template_name = "incidencias/espectador/agregar.html"
    success_url = reverse_lazy('incidencias_app:listar-espectador')


class detalleEspectador(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'incidencias/espectador/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Espectador


class ModificarEspectador(UpdateView):
    #le especificamos la tabla de BD
    model = Espectador
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'incidencias/espectador/agregar.html'
    success_url = reverse_lazy('incidencias_app:listar-espectador')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = EspectadorForm


class EliminarEspectador(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'incidencias/espectador/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Espectador
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('incidencias_app:listar-espectador')
