from django.contrib import admin
from .models import Partido, Resultados, Insidencia
# Register your models here.

admin.site.register(Partido)
admin.site.register(Resultados)
admin.site.register(Insidencia)
