# -*- encoding: utf-8 -*-
from django import forms
from apps.incidencias.models import Resultados, Partido,Insidencia, Libros, Espectador
from apps.equipo.models import Equipo
from django.db.models import Q


class ResultadosForm(forms.ModelForm):
    class Meta:
        model = Resultados
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super(ResultadosForm, self).__init__(*args, **kwargs)

        p = Partido.objects.filter(fixture__torneo__culminado = False)
        # realizamos consulta
        e1 = Equipo.objects.filter(estado=True)
        self.fields['partido'].queryset = p
        self.fields['equipo'].queryset = e1

class PartidoForm(forms.ModelForm):
    class Meta:
        model = Partido
        fields = ('__all__')

class IncidenciaForm(forms.ModelForm):
    class Meta:
        model = Insidencia
        fields = (
            'tipo',
            'minuto',
            'descripcion',
            'partido',
        )

class LibroForm(forms.ModelForm):
    class Meta:
        model = Libros
        fields = ('__all__')


class EspectadorForm(forms.ModelForm):
    class Meta:
        model = Espectador
        fields = ('__all__')

class BuscarForm(forms.Form):
    clave = forms.CharField(label='Ingrese Titulo', required=False)
