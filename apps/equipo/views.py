# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView
from django.views.generic.edit import FormView, FormMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy, reverse

from .forms import FacultadForm, EquipoForm, ComandoTecnicoForm ,JuntaDirectivaForm, PagoForm
from apps.equipo.models import Facultad, Equipo, ComandoTecnico, JuntaDirectiva, Pago
from apps.torneo.forms import PersonaForm, PrecioPago, Torneo
from apps.users.models import User

from django.http import HttpResponseRedirect
from django.utils import timezone


#manteimiento facultad
class ListFacultad(ListView):
    context_object_name = 'facultades'
    queryset = Facultad.objects.all()
    template_name = 'equipo/facultad/listar.html'


#clase para registrar un equipo utilizando createview
class RegistrarFacultad(CreateView):
    form_class = FacultadForm
    template_name = "equipo/facultad/agregar.html"
    success_url = reverse_lazy('equipo_app:listar-facultad')


class detalleFacultad(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'equipo/facultad/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Facultad


class ModificarFacultad(UpdateView):
    #le especificamos la tabla de BD
    model = Facultad
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'equipo/facultad/modificar.html'
    success_url = reverse_lazy('equipo_app:listar-facultad')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = FacultadForm


class EliminarFacultad(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'equipo/facultad/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Facultad
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('equipo_app:listar-facultad')


# modificar equipo
class ActualizarEquipo(SuccessMessageMixin, FormView):
    template_name = 'equipo/equipo/modificar.html'
    form_class = EquipoForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_form_kwargs(self):
        kwargs = super(ActualizarEquipo, self).get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
        })
        return kwargs

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ActualizarEquipo, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        equipo_query = Equipo.objects.filter(junta_directiva__presidente=usuario)
        if equipo_query.count() > 0:
            equipo = equipo_query[0]
            initial['nombre'] = equipo.nombre
            initial['color_camiseta'] = equipo.color_camiseta
            initial['junta_directiva'] = equipo.junta_directiva
            initial['comando_tecnico'] = equipo.comando_tecnico
            initial['facultad'] = equipo.facultad
            initial['logo'] = equipo.logo
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        equipo_query = Equipo.objects.filter(junta_directiva__presidente=usuario)
        if equipo_query.count() > 0:
            equipo = equipo_query[0]
            equipo.nombre = form.cleaned_data['nombre']
            equipo.color_camiseta = form.cleaned_data['color_camiseta']
            equipo.junta_directiva = form.cleaned_data['junta_directiva']
            equipo.comando_tecnico = form.cleaned_data['comando_tecnico']
            equipo.facultad = form.cleaned_data['facultad']
            equipo.logo = form.cleaned_data['logo']
            # guaranos el equipo con los nuevos datos
            equipo.save()
        return super(ActualizarEquipo, self).form_valid(form)


# metodo para ingresar un comando tecnico
class ModificarSecretario(SuccessMessageMixin, FormView):
    template_name = 'equipo/juntadirectiva/secretario.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarSecretario, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].secretario
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarSecretario, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].secretario
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].secretario
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                secre = form.save()
                # guardamos en junta directiva
                junta = secretario_query[0]
                junta.secretario = secre
                junta.save()
        return super(ModificarSecretario, self).form_valid(form)

class ModificarTesorero(SuccessMessageMixin, FormView):
    template_name = 'equipo/juntadirectiva/tesorero.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarTesorero, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].tesorero
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarTesorero, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].tesorero
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        secretario_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if secretario_query.count() > 0:
            persona = secretario_query[0].tesorero
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                tesorer = form.save()
                # guardamos en junta directiva
                junta = secretario_query[0]
                junta.tesorero = tesorer
                junta.save()
        return super(ModificarTesorero, self).form_valid(form)


# metodo para ingresar un comando tecnico
class ModificarTecnico(SuccessMessageMixin, FormView):
    template_name = 'equipo/comando_tecnico/tecnico.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarTecnico, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].tecnico
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarTecnico, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        print tecnico_query
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].tecnico
            print persona
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].tecnico
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                tecni = form.save()
                # guardamos en junta directiva
                comando = tecnico_query[0]
                comando.tecnico = tecni
                comando.save()
        return super(ModificarTecnico, self).form_valid(form)

class ModificarMedico(SuccessMessageMixin, FormView):
    template_name = 'equipo/comando_tecnico/medico.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarMedico, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].medico
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarMedico, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        medico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        print medico_query
        if medico_query.count() > 0:
            persona = medico_query[0].medico
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        medico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if medico_query.count() > 0:
            persona = medico_query[0].medico
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                medic = form.save()
                # guardamos en comando directiva
                comando = medico_query[0]
                comando.medico = medic
                comando.save()
        return super(ModificarMedico, self).form_valid(form)


class ModificarPreparador(SuccessMessageMixin, FormView):
    template_name = 'equipo/comando_tecnico/preparador.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarPreparador, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].preparador
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarPreparador, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        preparador_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        print preparador_query
        if preparador_query.count() > 0:
            persona = preparador_query[0].preparador
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        preparador_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if preparador_query.count() > 0:
            persona = preparador_query[0].preparador
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                prepa = form.save()
                print prepa.foto
                # guardamos en comando directiva
                comando = preparador_query[0]
                comando.preparador = prepa
                comando.save()
        return super(ModificarPreparador, self).form_valid(form)

class ModificarDelegado(SuccessMessageMixin, FormView):
    template_name = 'equipo/comando_tecnico/delegado.html'
    form_class = PersonaForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_context_data(self, **kwargs):
        context = super(ModificarDelegado, self).get_context_data(**kwargs)
        usuario = self.request.user
        # recuperamos las observaciones
        tecnico_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if tecnico_query.count() > 0:
            persona = tecnico_query[0].delegado
            if not persona is None:
                context['persona'] = persona
        context['persona'] = persona
        return context

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ModificarDelegado, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        delegado_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        print delegado_query
        if delegado_query.count() > 0:
            persona = delegado_query[0].delegado
            print persona
            if not persona is None:
                initial['dni'] = persona.dni
                initial['nombres'] = persona.nombres
                initial['apellidos'] = persona.apellidos
                initial['email'] = persona.email
                initial['direccion'] = persona.direccion
                initial['sexo'] = persona.sexo
                initial['telefono'] = persona.telefono
                initial['fecha_nacimineto'] = persona.fecha_nacimineto
                initial['foto'] = persona.foto
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        delegado_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if delegado_query.count() > 0:
            persona = delegado_query[0].delegado
            if not persona is None:
                persona.dni = form.cleaned_data['dni']
                persona.nombres = form.cleaned_data['nombres']
                persona.apellidos = form.cleaned_data['apellidos']
                persona.email = form.cleaned_data['email']
                persona.direccion = form.cleaned_data['direccion']
                persona.sexo = form.cleaned_data['sexo']
                persona.telefono = form.cleaned_data['telefono']
                persona.fecha_nacimineto = form.cleaned_data['fecha_nacimineto']
                persona.foto = form.cleaned_data['foto']
                # guaranos el equipo con los nuevos datos
                persona.save()
            else:
                form.save()
                delegado = form.save()
                print '=========Delegado==============='
                print delegado
                # guardamos en comando directiva
                comando = delegado_query[0]
                comando.delegado = delegado
                comando.save()
        return super(ModificarDelegado, self).form_valid(form)


class ActualizarComandoTecnico(SuccessMessageMixin, FormView):
    template_name = 'equipo/comando_tecnico/modificar.html'
    form_class = ComandoTecnicoForm
    success_url = reverse_lazy('users_app:panel_juntadirectiva')
    success_message = "Proceso Completado...!!"

    def get_initial(self):
        # recuperamos el objeto comando_tecnico
        initial = super(ActualizarComandoTecnico, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        comando_query = ComandoTecnico.objects.filter(junta_directiva__presidente=usuario)
        if comando_query.count() > 0:
            comando_tecnico = comando_query[0]
            initial['junta_directiva'] = comando_tecnico.junta_directiva
            initial['tecnico'] = comando_tecnico.tecnico
            initial['medico'] = comando_tecnico.medico
            initial['preparador'] = comando_tecnico.preparador
            initial['delegado'] = comando_tecnico.delegado
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        comando_query = Equipo.objects.filter(junta_directiva__presidente=usuario)
        if comando_query.count() > 0:
            comando_tecnico = comando_query[0]
            comando_tecnico.junta_directiva = form.cleaned_data['junta_directiva']
            comando_tecnico.tecnico = form.cleaned_data['tecnico']
            comando_tecnico.medico = form.cleaned_data['medico']
            comando_tecnico.preparador = form.cleaned_data['preparador']
            comando_tecnico.delegado = form.cleaned_data['delegado']
            # guaranos el comando_tecnico con los nuevos datos
            comando_tecnico.save()
        return super(ActualizarComandoTecnico, self).form_valid(form)

class ActualizarJuntaDirectiva(FormView):
    template_name = 'equipo/juntadirectiva/modificar.html'
    form_class = JuntaDirectivaForm
    success_url = '.'

    def get_initial(self):
        # recuperamos el objeto equipo
        initial = super(ActualizarJuntaDirectiva, self).get_initial()
        usuario = self.request.user
        # recuperamos las observaciones
        junta_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if junta_query.count() > 0:
            junta = junta_query[0]
            initial['presidente'] = junta.presidente
            initial['secretario'] = junta.secretario
            initial['tesorero'] = junta.tesorero
        return initial

    def form_valid(self, form):
        usuario = self.request.user
        # recuperamos las observaciones
        junta_query = JuntaDirectiva.objects.filter(presidente=usuario)
        if junta_query.count() > 0:
            junta = junta_query[0]
            junta.presidente = form.cleaned_data['presidente']
            junta.secretario = form.cleaned_data['secretario']
            junta.tesorero = form.cleaned_data['tesorero']
            # guaranos el junta con los nuevos datos
            junta.save()
        return super(ActualizarJuntaDirectiva, self).form_valid(form)


class ListPago(ListView):
    context_object_name = 'Pago'
    queryset = Pago.objects.filter(torneo__culminado=False)
    template_name = 'equipo/Pago/listar.html'


#clase para registrar un equipo utilizando createview
class RegistrarPago(DetailView):
    model = User
    template_name = "equipo/Pago/agregar.html"
    #success_url = reverse_lazy('equipo_app:listar-Pago')

    def get_context_data(self, **kwargs):
        context = super(RegistrarPago, self).get_context_data(**kwargs)
        #context['cantidad'] = self.object_list.count
        presidente = self.get_object()
        context['junta'] = JuntaDirectiva.objects.get(presidente=presidente)
        context['precio'] = PrecioPago.objects.filter(concepto='inscripcion')[0].monto
        context['torneo']= Torneo.objects.filter(culminado=False)[0]
        context['usuario']= self.request.user
        return context

    def post(self, request, *args, **kwargs):
        presidente = self.get_object()
        junta = JuntaDirectiva.objects.get(presidente=presidente)
        precio = PrecioPago.objects.filter(concepto='inscripcion')[0]
        torneo = Torneo.objects.filter(culminado=False)[0]
        usuario = self.request.user
        # guardamos el pago y actualizamos user
        pago = Pago(
            precio_pago=precio,
            junta_directiva=junta,
            torneo=torneo,
            fecha=timezone.now(),
            usuario=usuario,
        )
        presidente.is_active = True
        pago.save()
        presidente.save()
        return HttpResponseRedirect(
            reverse(
                'equipo_app:listar-Pago'
            )
        )


class detallePago(DetailView):
    # pasamos hatml donde se pintara el detalle del objeto
    template_name = 'equipo/Pago/detalle.html'
    #pasamos el modelo o tabla del objeto
    model = Pago


class ModificarPago(UpdateView):
    #le especificamos la tabla de BD
    model = Pago
    # le pasamos el template donde se pintaran los datos recureados
    template_name = 'equipo/Pago/modificar.html'
    success_url = reverse_lazy('equipo_app:listar-Pago')
    #le pasamos el formulario en el que recibiremos los datos a modificar
    form_class = PagoForm


class EliminarPago(DeleteView):
    #pasamos el template donde se mostraara los datos que vamos a eliminar
    template_name = 'equipo/Pago/eliminar.html'
    #pasamos el modelo del cual se eliminara elregistro
    model = Pago
    #donde ira cuando se complete la accion
    success_url = reverse_lazy('equipo_app:listar-Pago')

#######################LISTAR EQUIPOS EN FROTEND########################
class ListarEquipo(ListView):
    context_object_name = 'equipos'
    queryset = Equipo.objects.all()
    template_name = 'equipo/equipo-frotend/listar.html'
